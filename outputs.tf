output "creation_timestamp" {
  value = google_compute_subnetwork.tf-chapter12.creation_timestamp
}

output "gateway_address" {
  value = google_compute_subnetwork.tf-chapter12.gateway_address
}

output "self_link" {
  value = google_compute_subnetwork.tf-chapter12.self_link
}
